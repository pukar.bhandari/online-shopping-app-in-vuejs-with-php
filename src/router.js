import Vue from 'vue';
import VueRouter from 'vue-router';

import store from "./store/store";

import Login from "./components/login/Login";
import Register from "./components/register/Register";
import Shopping from "./components/shop/Shopping";
import ProductEdit from './components/shop/ProductEdit';
import ProductDetail from './components/shop/ProductDetail';
import Cart from './components/shop/Cart';
import Checkout from './components/shop/Checkout'

Vue.use(VueRouter);

const routes = [
    {
        path: '/login',
        component: Login,
        beforeEnter(to, from, next) {
            store.dispatch('tryAutoLogin');
            if(!store.state.username) {
                next()
            } if(store.state.username) {
                next('/shop')
            }
        }
    },
    {
        path: '/register',
        component: Register
    },
    {
        path: '/shop',
        component: Shopping,
        name: 'shop',
        beforeEnter(to, from, next) {
            store.dispatch('tryAutoLogin');
            if(store.state.username) {
                next()
            } if(!store.state.username){
                next('/login')
            }
        },
        children: [
            {
                path: 'add',
                component: ProductEdit
            },
            {
                path: 'details/:id',
                component: ProductDetail,
                name: 'detail'
            },
            {
                path: 'cart',
                component: Cart
            },
            {
                path: 'checkout',
                component: Checkout,
                beforeEnter(to, from, next) {
                    if(store.state.itemsInCart.length){
                        next()
                    }
                    if(!store.state.itemsInCart.length){
                        next('/shop/cart')
                    }
                }
            }
        ]
    },
    {
        path: '',
        redirect: '/shop'
    },
    {
        path: '**',
        redirect: '/shop'
    }
];

export default new VueRouter({mode: 'history', routes})