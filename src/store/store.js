import Vue from 'vue';
import Vuex from 'vuex';

import router from '../router';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        username: null,
        itemsInCart: [],
        totalAmount: null
    },
    mutations: {
        authUser (state, userData) {
            state.username = userData.username;
        },
        clearAuthData (state) {
            state.username = null;
        },
        addItems(state, items) {
            state.itemsInCart.push(items);
        },
        setTotalAmount(state, amount) {
            state.totalAmount = amount;
        }
    },
    actions: {
        addItems({commit},items){
            commit('addItems', items);
        },
        login({commit},  username) {
            localStorage.setItem('username', username);
            commit('authUser', {
                username: username
            })
        },
        tryAutoLogin({commit}) {
            const username = localStorage.getItem('username');
            if(!username) {
                return
            }
            commit('authUser', {
                username: username
            })
        },
        logout({commit}) {
            commit('clearAuthData');
            axios.get('http://localhost:8000/api/logout.php')
                .then(() => {
                    localStorage.removeItem('username');
                    router.replace('/login');
                }).catch((err) => {
                console.log(err);
            });
        }
    },
    getters: {
        username(state) {
            return state.username;
        },
        isAuthenticated(state) {
          return state.username !== null
        },
        itemsInCart(state){
            return state.itemsInCart;
        },
        totalAmount(state) {
            return state.totalAmount;
        }
    }
})