import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import router from './router';

import store from "./store/store";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

import Vuelidate from "vuelidate";

Vue.use(Vuelidate);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
