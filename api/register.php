<?php
require_once("initialize.php");

$db = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
if (mysqli_connect_errno()) {
    die("Database connection failed:" .
        mysqli_connect_error() .
        " (". mysqli_connect_errno() . ")"
    );
}

$method = $_SERVER['REQUEST_METHOD'];
$username = '';
$password = '';
$first_name = '';
$last_name = '';
$email = '';
$phone_number = '';

if ($method === 'POST') {
    $ok = true;
    if (!isset($_POST['username']) || $_POST['username'] === '') {
        $ok = false;
    } else {
        $username = $_POST['username'];
    }

    if (!isset($_POST['password']) || $_POST['password'] === '') {
        $ok = false;
    } else {
        $password = $_POST['password'];
    }

    if (!isset($_POST['first_name']) || $_POST['first_name'] === '') {
        $ok = false;
    } else {
        $first_name = $_POST['first_name'];
    }

    if (!isset($_POST['last_name']) || $_POST['last_name'] === '') {
        $ok = false;
    } else {
        $last_name = $_POST['last_name'];
    }

    if (!isset($_POST['email']) || $_POST['email'] === '') {
        $ok = false;
    } else {
        $email = $_POST['email'];
    }

    if (!isset($_POST['phone_number']) || $_POST['phone_number'] === '') {
        $ok = false;
    } else {
        $phone_number = $_POST['phone_number'];
    }

    if ($ok) {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $sql = sprintf("INSERT INTO users (username, hashed_password,first_name, last_name, email, phone_number) VALUES ('%s', '%s', '%s','%s', '%s', '%s')",
            $db->real_escape_string($username),
            $db->real_escape_string($hash),
            $db->real_escape_string($first_name),
            $db->real_escape_string($last_name),
            $db->real_escape_string($email),
            $db->real_escape_string($phone_number)
        );
        $result = $db->query($sql);
        if (!$result) {
            die("Database query failed.");
        }
        echo "<p>User added</p>";
        $db->close();
    } if(!$ok) {
        http_response_code(400);
        die('Parameters missing');
    }
}
?>