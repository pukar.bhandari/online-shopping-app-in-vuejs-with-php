<?php
require 'initialize.php';

class Upload
{
    public $product_name;
    public $description;
    public $price;
    public $filename;
    public $posted_date;
    public $username;

    private $connection;
    private $temp_path;
    protected $upload_dir = "images";
    public $errors = array();

    protected $upload_errors = array(
        UPLOAD_ERR_OK => "No errors.",
        UPLOAD_ERR_INI_SIZE => "Larger than upload_max_filesize.",
        UPLOAD_ERR_FORM_SIZE => "Larger than form MAX_FILE_SIZE.",
        UPLOAD_ERR_PARTIAL => "Partial upload.",
        UPLOAD_ERR_NO_FILE => "No file.",
        UPLOAD_ERR_NO_TMP_DIR => "No temporary directory.",
        UPLOAD_ERR_CANT_WRITE => "Can't write to disk.",
        UPLOAD_ERR_EXTENSION => "File upload stopped by extension."
    );

    public function attach_file($file)
    {
        if (!$file || empty($file) || !is_array($file)) {
            $this->errors[] = "No file was uploaded.";
            return false;
        } elseif ($file['error'] != 0) {
            $this->errors[] = $this->upload_errors[$file['error']];
            return false;
        } else {
            $this->temp_path = $file['tmp_name'];
            $this->filename = basename($file['name']);
            return true;
        }
    }

    public function save()
    {
        if (!empty($this->errors)) {
            return false;
        }

        if (empty($this->filename) || empty($this->temp_path)) {
            $this->errors[] = "The file location was not available.";
            return false;
        }

        $target_path = SITE_ROOT . DS . 'api' . DS . $this->upload_dir . DS . $this->filename;

        if (file_exists($target_path)) {
            $this->errors[] = "The file {$this->filename} already exists.";
            return false;
        }

        if (move_uploaded_file($this->temp_path, $target_path)) {
            if ($this->create()) {
                unset($this->temp_path);
                return true;
            }
        } else {
            $this->errors[] = "The file upload failed, possibly due to incorrect permissions on upload folder.";
            return false;
        }
    }

    public function image_path() {
        return $this->upload_dir.DS.$this->filename;
    }

    public function create() {
        $this->open_connection();
        $dt = time();
        $this->posted_date = strftime("%Y-%m-%d %H:%M:%S", $dt);
        $query = sprintf("SELECT id FROM users WHERE username='%s'",
            $this->connection->real_escape_string($this->username));
        $result = $this->connection->query($query);
        $user_id = 0;
        if($result) {
            $row = mysqli_fetch_array($result);
            $user_id = $row['id'];
        }
        $sql = sprintf("INSERT INTO products (product_name, description,price, filename, posted_date, user_id) VALUES ('%s', '%s', '%s','%s', '%s', %d)",
            $this->connection->real_escape_string($this->product_name),
            $this->connection->real_escape_string($this->description),
            $this->connection->real_escape_string($this->price),
            $this->connection->real_escape_string($this->filename),
            $this->connection->real_escape_string($this->posted_date),
            $user_id
        );
        if($this->connection->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function open_connection() {
        $this->connection =  new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        if (mysqli_connect_errno()) {
            die("Database connection failed:" .
                mysqli_connect_error() .
                " (". mysqli_connect_errno() . ")"
            );
        }
    }
}


?>