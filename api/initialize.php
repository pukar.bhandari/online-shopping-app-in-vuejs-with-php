<?php

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
defined('SITE_ROOT') ? null:
    define('SITE_ROOT', DS.'Users'.DS.'pukar'.DS.'vue-npm'.DS.'online-shopping-app-in-vuejs-with-php');

defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'api');

require_once(LIB_PATH.DS."config.inc.php");
require_once(LIB_PATH.DS."session.php");
require_once(LIB_PATH.DS."user.php");

?>