<?php
require_once("initialize.php");

$db = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
$sql = "SELECT * FROM products";
$result = $db->query($sql);
$object_array= [];
while ($row = $result->fetch_object()) {
    $object_array[] = $row;
}
echo json_encode($object_array);


?>