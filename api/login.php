<?php
require_once("initialize.php");
?>

<?php

if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    $found_user = User::authenticate($username, $password);

    if ($found_user) {
        $session->login($found_user);
    } else {
        http_response_code(400);
        die('Username/password combination incorrect. ');
    }
} else {
    $username = "";
    $password = "";
}

?>