<?php
require 'initialize.php';
require_once(LIB_PATH.DS."upload.php");
?>

<?php
$method = $_SERVER['REQUEST_METHOD'];
if($method == 'POST') {
    $product = new Upload();
    $product->product_name = $_POST['product_name'];
    $product->description = nl2br($_POST['description']);
    $product->price = $_POST['price'];
    $product->username = $_POST['username'];
    $product->attach_file($_FILES['image']);
    if($product->save()) {
        echo "Product added successfully";
    } else {
        print_r($product->errors);
    }
}
?>
