<?php
require_once("initialize.php");

$db = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
if (mysqli_connect_errno()) {
    die("Database connection failed:" .
        mysqli_connect_error() .
        " (". mysqli_connect_errno() . ")"
    );
}

$method = $_SERVER['REQUEST_METHOD'];
$username = '';
$items_in_cart = '';
$total_amount = '';
$delivery_address = '';

if ($method === 'POST') {
    $ok = true;
    if (!isset($_POST['username']) || $_POST['username'] === '') {
        $ok = false;
    } else {
        $username = $_POST['username'];
    }

    if (!isset($_POST['items_in_cart']) || $_POST['items_in_cart'] === '') {
        $ok = false;
    } else {
        $items_in_cart = $_POST['items_in_cart'];
    }

    if (!isset($_POST['total_amount']) || $_POST['total_amount'] === '') {
        $ok = false;
    } else {
        $total_amount = $_POST['total_amount'];
    }

    if (!isset($_POST['delivery_address']) || $_POST['delivery_address'] === '') {
        $ok = false;
    } else {
        $delivery_address = $_POST['delivery_address'];
    }

    if ($ok) {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $query = sprintf("SELECT id FROM users WHERE username='%s'",
            $db->real_escape_string($username));
        $user_result = $db->query($query);
        $customer_id = 0;
        if($user_result) {
            $row = mysqli_fetch_array($user_result);
            $customer_id = $row['id'];
        }
        $sql = sprintf("INSERT INTO orders (items_in_cart, total_amount, delivery_address, customer_id) VALUES ('%s', '%s', '%s', %d)",
            $db->real_escape_string($items_in_cart),
            $db->real_escape_string($total_amount),
            $db->real_escape_string($delivery_address),
            $customer_id
        );
        $result = $db->query($sql);
        if (!$result) {
            die("Database query failed.");
        }
        echo "<p>Order added successfully.</p>";
        $db->close();
    } if(!$ok) {
        http_response_code(400);
        die('Parameters missing');
    }
}
?>